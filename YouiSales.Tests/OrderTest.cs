﻿using System;
using System.Runtime.InteropServices.ComTypes;
using NUnit.Framework;
using Shouldly;

namespace YouiSales.Tests
{
    [TestFixture]
    public class OrderTest
    {
        private readonly static Policy BMW = new Policy("Jane Doe", "BMW", Constants.Car);
        private readonly static Policy Harley = new Policy("John Doe", "Harley", Constants.Motorcycle);
        private readonly static Policy SunnyCoast = new Policy("John Doe", "Sunshine Coast", Constants.Home);

        [Test]
        public void ReceiptOneBMW()
        {
            var order = new Order("Youi");
            order.AddLine(new Line(BMW, 1));
            order.Receipt(Constants.ReceiptType.Text).ShouldBe(ResultStatementOneBMW);
        }

        private string ResultStatementOneBMW = @"Order Receipt for Youi" + Environment.NewLine + @" 1 x Jane Doe BMW = $105.00" + 
            Environment.NewLine + "Sub-Total: $105.00" + Environment.NewLine + "Tax: $10.50"
            + Environment.NewLine + "Total: $115.50" + Environment.NewLine + "Date: " + DateTime.Now.ToString("F");

        [Test]
        public void ReceiptOneHarley()
        {
            var order = new Order("Youi");
            order.AddLine(new Line(Harley, 1));
            order.Receipt(Constants.ReceiptType.Text).ShouldBe(ResultStatementOneHarley);
        }

        private string ResultStatementOneHarley = @"Order Receipt for Youi" + Environment.NewLine + 
            @" 1 x John Doe Harley = $56.00" + Environment.NewLine + 
"Sub-Total: $56.00" + Environment.NewLine +
"Tax: $5.60" + Environment.NewLine + 
"Total: $61.60" + Environment.NewLine + 
"Date: " + DateTime.Now.ToString("F");

        [Test]
        public void ReceiptOneSunnyCoast()
        {
            var order = new Order("Youi");
            order.AddLine(new Line(SunnyCoast, 1));
            order.Receipt(Constants.ReceiptType.Text).ShouldBe(ResultStatementOneSunnyCoast);
        }

        private static readonly string ResultStatementOneSunnyCoast = @"Order Receipt for Youi" + Environment.NewLine +
	@" 1 x John Doe Sunshine Coast = $235.00" + Environment.NewLine +  
"Sub-Total: $235.00" + Environment.NewLine +
"Tax: $23.50" + Environment.NewLine +
"Total: $258.50" + Environment.NewLine +
"Date: " + DateTime.Now.ToString("F");

        [Test]
        public void HtmlReceiptOneBMW()
        {
            var order = new Order("Youi");
            order.AddLine(new Line(BMW, 1));
            order.Receipt(Constants.ReceiptType.Html).ShouldBe(HtmlResultStatementOneBMW);
        }

        private string HtmlResultStatementOneBMW = @"<html><body><h1>Order Receipt for Youi</h1><ul><li>1 x Jane Doe BMW = $105.00</li></ul><h3>Sub-Total: $105.00</h3><h3>Tax: $10.50</h3><h2>Total: $115.50</h2><h3>Date: " + DateTime.Now.ToString("F") + "</h3></body></html>";

        [Test]
        public void HtmlReceiptOneHarley()
        {
            var order = new Order("Youi");
            order.AddLine(new Line(Harley, 1));
            order.Receipt(Constants.ReceiptType.Html).ShouldBe(HtmlResultStatementOneHarley);
        }

        private string HtmlResultStatementOneHarley = @"<html><body><h1>Order Receipt for Youi</h1><ul><li>1 x John Doe Harley = $56.00</li></ul><h3>Sub-Total: $56.00</h3><h3>Tax: $5.60</h3><h2>Total: $61.60</h2><h3>Date: " + DateTime.Now.ToString("F") + "</h3></body></html>";

        [Test]
        public void HtmlReceiptOneSunnyCoast()
        {
            var order = new Order("Youi");
            order.AddLine(new Line(SunnyCoast, 1));
            order.Receipt(Constants.ReceiptType.Html).ShouldBe(HtmlResultStatementOneSunnyCoast);
        }

        private string HtmlResultStatementOneSunnyCoast = @"<html><body><h1>Order Receipt for Youi</h1><ul><li>1 x John Doe Sunshine Coast = $235.00</li></ul><h3>Sub-Total: $235.00</h3><h3>Tax: $23.50</h3><h2>Total: $258.50</h2><h3>Date: " + DateTime.Now.ToString("F") + "</h3></body></html>";
    }
}
