﻿using System;

namespace YouiSales
{
    /// <summary>
    /// Class for member policy
    /// </summary>
    public class Policy
    {
        public string PolicyHolderName { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public Policy(string policyHolderName, string description, int price)
        {
            PolicyHolderName = policyHolderName;
            Description = description;
            Price = price;
        }
    }
}
