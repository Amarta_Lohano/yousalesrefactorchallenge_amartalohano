﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Transactions;
using static YouiSales.Constants;

namespace YouiSales
{
    /// <summary>
    /// Class to Process Orders
    /// </summary>
    public class Order
    {
        private readonly IList<Line> _lines = new List<Line>();
        public string Company { get; set; }

        public Order(string company)
        {
            Company = company;
        }

        
        public void AddLine(Line line)
        {
            _lines.Add(line);
        }
        /// <summary>
        /// Calculate price for product
        /// </summary>
        /// <param name="quantity">quantity of the item</param>
        /// <param name="price">price of the item</param>
        /// <param name="discount">percentage of discount</param>
        /// <returns></returns>
        private double CalculatePrice(int quantity, int price, double discount)
        {
            return (double) quantity > 1 ? quantity * price * discount : quantity * price;
        }
        /// <summary>
        /// Add all the placeholder need to be replaced into dictionary
        /// </summary>
        /// <param name="templateString"></param>
        /// <param name="placeHolders"></param>
        /// <returns></returns>
        private string ReplacePlaceHolders(string templateString, Dictionary<string, string> placeHolders)
        {
            foreach (var currentPlaceHolder in placeHolders)
            {
                templateString = templateString.Replace(currentPlaceHolder.Key, currentPlaceHolder.Value);
            }
            return templateString;
        }
        /// <summary>
        /// Generate the reports based on report Type
        /// </summary>
        /// <param name="pReceiptType">Report Type of<see cref="ReceiptType"/></param>
        /// <returns></returns>
        public string Receipt(ReceiptType pReceiptType)
        {
            Logger.Instance.LogInformation($"Printing receipt ({pReceiptType.ToString()} version) - Start");
            Dictionary<string, string> placeHolders = new Dictionary<string, string>();
            var totalAmount = 0d;
            placeHolders.Add("{Company}", Company);
            string orderLines = string.Empty;
            foreach (var line in _lines)
            {
                var thisAmount = 0d;
                if (line.Policy.Price == Constants.Car)
                {
                    thisAmount += CalculatePrice(line.Quantity, line.Policy.Price, Constants.Discount10Percentage);
                }
                else if (line.Policy.Price == Constants.Motorcycle)
                {
                    thisAmount += CalculatePrice(line.Quantity, line.Policy.Price, Constants.Discount20Percentage);
                }
                else if (line.Policy.Price == Constants.Home)
                {
                    thisAmount += CalculatePrice(line.Quantity, line.Policy.Price, Constants.Discount20Percentage);
                }

                orderLines += (pReceiptType == ReceiptType.Html ? "<li>" : " ") + Constants.Line.Replace("{PolicyHolderName}", line.Policy.PolicyHolderName)
                    .Replace("{Description}", line.Policy.Description)
                    .Replace("{Amount}", thisAmount.ToString("C"))
                    .Replace("{Quantity}", line.Quantity.ToString()) + (pReceiptType == ReceiptType.Html ? "</li>" : "");

                totalAmount += thisAmount;
            }
            placeHolders.Add("{OrderLines}",orderLines);
            placeHolders.Add("{TotalAmount}", totalAmount.ToString("C"));
            var tax = totalAmount * Constants.TaxRate;
            placeHolders.Add("{Tax}", tax.ToString("C"));
            placeHolders.Add("{TotalAmountWithTax}", (totalAmount + tax).ToString("C"));
            placeHolders.Add("{DateTime}", DateTime.Now.ToString("F"));
            Logger.Instance.LogInformation($"Printing receipt ({pReceiptType} version) - Finish");
            return pReceiptType == ReceiptType.Html ? ReplacePlaceHolders(Constants.HtmlReceiptTemplate, placeHolders) : ReplacePlaceHolders(Constants.TextReceiptTemplate, placeHolders);
        }
    }
}