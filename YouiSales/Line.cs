﻿namespace YouiSales
{
    /// <summary>
    /// Class to store line item for order
    /// </summary>
    public class Line
    {
        public Policy Policy { get; set; }
        public int Quantity { get; set; }
        public Line(Policy policy, int quantity)
        {
            Policy = policy;
            Quantity = quantity;
        }
    }
}