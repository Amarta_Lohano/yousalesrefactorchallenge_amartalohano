﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YouiSales
{
    /// <summary>
    /// Includes constant used through out project 
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// Price for car policy
        /// </summary>
        public const int Car = 105;

        /// <summary>
        /// Price for Motorcycle policy
        /// </summary>
        public const int Motorcycle = 56;

        /// <summary>
        /// Price for home policy
        /// </summary>
        public const int Home = 235;

        /// <summary>
        /// Current Tax rate
        /// </summary>
        public const double TaxRate = .1d;
        /// <summary>
        /// 10 % discount
        /// </summary>
        public const double Discount10Percentage = .9d;
        /// <summary>
        /// 20% discount
        /// </summary>
        public const double Discount20Percentage = .8d;

        //templates for receipts
        public static readonly string Line = "{Quantity} x {PolicyHolderName} {Description} = {Amount}";
        public static readonly string TextReceiptTemplate = @"Order Receipt for {Company}
{OrderLines}
Sub-Total: {TotalAmount}
Tax: {Tax}
Total: {TotalAmountWithTax}
Date: {DateTime}";

        public const string HtmlReceiptTemplate = "<html><body><h1>Order Receipt for {Company}</h1><ul>{OrderLines}</ul>" +
                                                    "<h3>Sub-Total: {TotalAmount}</h3>" +
                                                    "<h3>Tax: {Tax}</h3>" +
                                                    "<h2>Total: {TotalAmountWithTax}</h2>" +
                                                    "<h3>Date: {DateTime}</h3>" +
                                                    "</body></html>";

        public enum ReceiptType{
            Html,
            Text
        }
    }
}
